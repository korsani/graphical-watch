# graphical-watch

When watch(1) meet Grafana

Have you ever wanted watch(1) to graph command's output? Or you sometimes want to graph shell script output and you don't want to install Grafana? or hassle with gnuplot(1)?

This pure shell script is for you!

Unlike the nice [gwatch](https://github.com/robertely/gwatch), it is pure shell, and colored, and can take from stdin, and has more options.

	grwatch.sh -n 1 -r -l 100 -u 250 -t "Ping to google, 100x" "ping -c 1 www.google.com | grep 'time=' | awk '{print \$8}' | tr -cd '[0-9]' | xargs"

![Ping to google, zoomed x100](/doc/ping-to-google.png)

# Starting

## Pre-requisite

- A fairly recent bash(1)
- bc(1)
- Eventualy jq(1) and jo(1)

## Installation

Copy grwatch.sh(1) somewhere accessible by your PATH, or set PATH so that it finds grwatch.sh(1)

# Running

## Options

	grwatch.sh [ --sleep <interval in second> | --window-width <width in second> ] [ --zero <value> ] [ -a <suffix> ] [ --file <file> ] [ --rainbow ] [ --mark <mark> | --matrix ] [ --title <command title> ] [ [ --lower <lower bound> --upper <upper bound> ] | --scale-factoe <scale> ] [ "command than returns values" | --df <directory> ]
	grwatch.sh --continue --file <file>
	grwatch.sh --replay --file <file>

	-a : append a suffix to the value displayed
	--continue -c : continue: load data (values, command) and options (--slope, --mark, --sleep, --scale-factor, --title, --lower, --upper) from the --file file generated in a previous run, then run
	--df : Command run is "df --block-size 1 <directory>", data gathered is the fourth field: free space, use "--unit" M (unless overridden)
	--du : Command run is "du --summarize --block-size 1 <directory>", use "--unit" M (unless overridden)
	--dump-period -p : period: dump data every that number of dot. Default is every 5 dot. Valid only with --file
	--file -f : dump data in that file upon exit or when SIGHUP is received
	--lower -l : set lower value
	--mark -m : use that one-char string to display mark
	--matrix -x : cyberpunk style: --rainbow and mark is random
	--rainbow -r : rainbow mode
	--replay -e : replay dump specified by --file
	--scale-factor -s : scale. One line height in the term will count for that many dot. Set to < 1 to zoom in, > 1 to zoom out. Default is 1
	--sleep -n : sleep that seconds between each dot. May be decimal. Default is 2s
	--slope -d : diff/derivate: graph the slope between two dots ( (y2-y1)/sleep ) instead of dots value
	--title -t : display that title string in status bar instead of the command
	--upper -u : set upper value
	--window-width -w : width: set the duration of a screen to that many seconds, compute --sleep accordingly
	--zero -0 : set the horizontal axis to that value instead of 0. If not specified, it will be the first input value

If ```--title``` and ```--upper``` are provided, ```--scale-factor``` and ```--zero``` are ignored.

Last one of ```--sleep``` and ```--window-width``` win.

Command is run by ```sh -c```

## Examples

Act like watch(1):

	# Run 'pgrep -c php-fpm' every 20s
	$ grwatch.sh -n 20 "pgrep -c php-fpm"

Act like Grafana:

	# Take values from stdin
	$ while true ; do pgrep -c php-fpm ; sleep 20 ; done | grwatch.sh
	# Exit by ctrl+c or by sending EOF to stdin

Act like gnuplot(1):

	# Generate 100 random numbers between 0 and 100
	for i in {0..100} ; do echo $((RANDOM%100)) ; done > /tmp/random
	# Graph it
	grwatch.sh -l 0 -u 100 -x < /tmp/random

More examples:

	# Memory usage:
	$ grwatch.sh -n 0.5 -s 100 'free | grep -i mem | awk "{print \$3}"'

	# CPU temp:
	$ grwatch.sh -n 0.5 -0 43 -t "Temp of thermal0" 'cat /sys/class/thermal/thermal_zone0/temp | cut -c -2'
	# ... on rpi
	$ grwatch.sh -0 43 -r -t "🔥 raspberry" "/opt/vc/bin/vcgencmd measure_temp | tr -cd '0-9.'"

	# Show a rainbow bubble sine:
	$ for i in {0..180} ; do LANG=C LC_ALL=C printf '%0.0f\n' "$(bc -l <<<"scale=2;20*s($i*(2*6.28)/360)")" ; done | grwatch.sh -n 0 -r -m 'o' -t sine

	# Monitor number of php-fpm processes on a 1-hour graph:
	$ grwatch.sh -w 3600 -r -m '-' 'pgrep -c php-fpm'

	# Monitor your ping, values are x100, dump data upon exit
	$ grwatch.sh -f ping-google.json "ping -c 1 www.google.com | grep 'time=' | awk '{print \$8}' | tr -cd '[0-9]' | xargs"

	# Continue the previous run
	$ grwatch.sh -c -f ping-google.json

	# Replay the session
	$ grwatch.sh -e -f ping-google.json

	# Show logs
	$ jq -r '.logs | sort_by(.ts_nano) | .[] | (.ts_nano|tostring)+"\t"+.value' ping-google.json

# Considerations

## Status

There is a status zone on the top left of the screen, like:

	Every 10s: ○ Temp of thermal0                                                                              Cygnus: 2022-09-27@00:25:30
	25.00 m=25 M=43 width=119.0s tick=2.5s n=0.50s scale=1.0x x=19 y=47.00 file=dump.json pid=666

First line is the periodicity, the mode and the command (or the string you gave with ```-t ``` )

Mode is:

    - ○ for "on air": ```-f``` and ```-e``` were not specified (values are not recorded, and was is graphed is what is given by the command)
    - ● for "record": ```-f``` was specified (values are recorded, and what is graphed is what is given by the command)
    - ▶ for "play": ```-e```was specified (what is graphed is what was recorded and read by ``-f```)

Second line is:

- 25: current value
- m=25: min value, rounded to int
- M=43: max value, rounded to int
- width=119.0s : width of the screen, in seconds
- tick=2.5s : time between each x tick on axis
- scale=1.0x : scale
- x=19 y=47.00 : x and y coordinate (in column/line)
- file=dump.json : file values will dumped to upon exit, if asked so
- pid=666 : pid of running instance (useful for kill -1)

Hostname and date are shown on upper right corner.

## Autoscale

Scale is recalculated if value exceed screen bounds. Graph is redraw with the new scale.

Starting value is kept. That means that graph is not recentered, just zoomed in or out.

## Dump

Dump is made in json format. It contains enough info to fully replay a run:

	for i in {0..180} ; do LANG=C printf '%0.0f\n' "$(bc -l <<<"scale=2;20*s($i*(2*6.28)/360)")" ; done | grwatch.sh -n 0 -r -m '~' -f sine.json
	jq '.infos' sine.json
	jq '.data | .[]' sine.json | grwatch.sh

If you interrupted a run you made with -f and want to relaunch it without loosing previous data, run with ```-c```.

# To do

- Allow y axis to be elsewhere than in the middle
- Improve speed
- Find why my utf8 dot is not well displayed on some terminal ('kitty' is ok, not iTerm2, not Gnome Terminal)
- Handle screen resizing

# Made with

- bash(1)
- bc(1)
- shellcheck(1)
- bits of jq(1) and jo(1)
- love, patience, and colors.

# See also

- bc(1), jq(1), jo(1)
- Grafana
- [A nice list of term codes](https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797)
- [UTF8 table and search](https://unicode-table.com/fr)

