#!/usr/bin/env bash
set -eu
LANG=C
LC_ALL=C
ME="grwatch.sh"
export PS4='>[${FUNCNAME:-(main)}] $LINENO '
export LINES="$(tput lines)"
export COLUMNS="$(tput cols)"
# █■⯀▪·
MARK_DEFAULT=('⯀')
# Of course...
case $(uname) in
	Darwin)		MARK_TIP='⬥'; MARK_DEFAULT=('■') ; date=date ;;
	FreeBSD)	date=gdate ;;
	*)			MARK_TIP='◆'; date=date ;; # MARK_DEFAULT='🔲' ;;
esac
MARK_TIP='X'
declare -A COLORS LOGS MODES
COLORS=( [white]="\e[1;37m" [cyan]="\e[36m" [underlined]="\e[4;37m" [reset]="\e[0m" [dim]="\e[2m" [bg_red]="\e[41m" [bg_blue]="\e[44m" [bg_green]="\e[42m" [gray]="\e[37m" [bg_purple]="\e[45m" )
MODES=( [onair]="${COLORS[bg_purple]}🎙️${COLORS[reset]}" [record]="${COLORS[bg_red]}●${COLORS[reset]}" [play]="${COLORS[bg_green]}▶${COLORS[reset]}" )
MODE="onair"
MARK_COLOR="255;255;255"
TICK_COLOR=184
HLINE_COLOR=053
SLEEP_DEFAULT=2
DT=0
SUFFIX_DEFAULT=''
scale_factor_default=1
command_title_default=''
HEADER_SIZE=2
X_TICK='|'
X_TICKS_STEP=5
Y_TICKS_STEP=5
KEEP_LOG_FILE='/tmp/.grwatch_keep-log'
SLEEP_KEEP_SECONDS=5
# y ticks position relative to center
Y_TICKS_RELPOS=( $(seq $(( (-LINES+HEADER_SIZE*Y_TICKS_STEP)/2 )) $Y_TICKS_STEP $(( (LINES-HEADER_SIZE*Y_TICKS_STEP)/2 )) ) )
# central line position
lcenter="$(( LINES-(LINES-HEADER_SIZE)/2))"
hcenter="$((COLUMNS/2))"
# absolute position of y ticks
Y_TICKS_LABSPOS=( $(seq $(( (lcenter+(-LINES+HEADER_SIZE*Y_TICKS_STEP)/2) )) $Y_TICKS_STEP $(( (lcenter+(LINES-HEADER_SIZE*Y_TICKS_STEP)/2) )) ) )
#tracer_pos_line=$((LINES-1))
HOSTNAME="$(hostname)"
# 2022-09-28T15:48:50+02:00
DATE_COLUMNS="$((COLUMNS-25-${#HOSTNAME}-1))"
START_TIME="$($date +%s)"
RAINBOW=''
INT=''
UNIT=''
declare -A UNITS=( [k]=1 [M]=2 [G]=3 [T]=4 [P]=5 [E]=6 [Z]=7 [Y]=8 [R]=9 [Q]=10 )
command=''
start_value=''
last_x=''
last_y=''
last_mc=''
MIN=''
MAX=''
DUMP_FILE=''
DUMP_PERIOD=5
CONTINUE=''
REPLAY=''
DER=''
# File to store values
VALUES_FILE="$(mktemp "/tmp/grwatch.XXXXXX")"
LOG_DUMP_FILE="$(mktemp "/tmp/grwatch.log.XXXXXX")"
dotv=( 0 ) ; dot=( 0 ) ; values=( 0 )
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function color_me() {
	echo -ne "${COLORS[$1]}$2${COLORS[reset]}"
}
function _check_command() {
	while [ -n "${1:-}" ] ; do
		if ! command -v $1 >/dev/null ; then
			echo "Command '$1' not found" >&2
			return 1
		fi
		shift
	done
}
# Some checks
function preflight_check() {
	_check_command bc jo
}
# Display a string of the last line
function _log() {
	local msg="[\e[2m$($date '+%H:%M:%S')\e[0m] $1"
	echo -ne "\e[$LINES;1H$msg\e[0K\e[0m"
	source "$LOG_DUMP_FILE"
	if [ -n "$1" ] ; then
		LOGS[$($date "+%s.%N")]="$1"
	fi
	# bash(1) doesnt allow to modify global var within a function
	# So I dump the "code" of that var to rebuild it next time #dirty
	declare -p LOGS > "$LOG_DUMP_FILE"
	# If I'm asked to keep log message a bit longer...
	(sleep $SLEEP_KEEP_SECONDS ; while [ -e "$KEEP_LOG_FILE" ] ; do sleep 1 ; done ; echo -en "\e[${LINES};1H\e[2K" ) &
}
# Return true if $1 is a float
function is_float() {
	local v="$1"
	[[ "$v" =~ ^\ *[-+]?[0-9]*[.,]*[0-9]+$ ]]
}
function s2dhms() {
	local w="$1"
	bc <<< "yw=$w/31556952;w=$w%31556952;dw=w/86400;w=w%86400;hw=w/3600;w%=3600;mw=w/60;w%=60;
	if(yw!=0) print yw,\"y\";
	if(dw!=0) print dw,\"d\";
	if(hw!=0) print hw,\"h\";
	if(mw!=0) print mw,\"min\";
	if(w!=0) print w,\"s\";"
}
# Generate the rgb dot of the hues of the chromatic circle, by steps of $1
# Running chromatic circle make rgb value vary this way:
# R   G   B
# 255 0   0
# |   +   |
# 255 255 0
# -   |   |
# 0   255 0
# |   |   +
# 0   255 255
# |   -   |
# 0   0   255
# +   |   |
# 255 0   255
# |   |   -
# 255 0   0
function generate_rainbow() {
	# Order of the colors I will increment/decrement
	local order='bgrbgr'
	# Way I will increment/decrement
	local sign=1 step="$1"
	# Staring dot
	declare -A RGB
	RGB[r]=0 ; RGB[g]=255 ; RGB[b]=0
	for i in $(seq 0 $(( ${#order}-1)) ) ; do	# r or g or b
		c=${order:$i:1}					# index in the array
		v=${RGB[$c]}					# starting value
		while [ $v -ge 0 ] && [ $v -le 255 ] ; do	# Am I always in the bounds?
			RGB[$c]="$v"
			echo "${RGB[r]};${RGB[g]};${RGB[b]}"
			v="$(( v+(sign*step) ))"		# increment ot decrement
		done
		RGB[$c]="$(( (sign+1)*255/2 ))"	# set to 0 or 255
		sign="$((sign*-1))"				# flip/flop
	done
}
# Draw the x line, at coord $1
function h_line() {
	local y="$1"
	echo -ne "\e[$y;1H\e[38;5;${HLINE_COLOR}m"
	printf -- '―%.0s' $(seq 1 "$COLUMNS")
	disp_x_ticks "$y"
	echo -ne "\e[0m"
	disp_y_labels
}
function has_units() {
	local u="$1"
	[ -n "$u" ] && [[ kmgtpezyrq =~ .*${u,,}.* ]]
}
function format_unit() {
	local v="$1" u="$2"
	u="${u,,}"
	if [ "$u" != "k" ] ; then
		u="${u^^}"
	fi
	printf "%0.01f$u" "$(bc <<< "scale=1;$v/(1024^${UNITS[${u}]})")"
}
# Displays the y ticks and label, eventually at line $1
function disp_y_labels() {
	local y="${1:-}"		# absolute position of the tick
	local ticks l y
	echo -ne "\e[38;5;${TICK_COLOR}m"
	# one tick ? many ticks ?
	if [ -n "$y" ] ; then
		ticks=( "$y" )
	else
		ticks=( "${Y_TICKS_LABSPOS[@]}" )
	fi
	#echo "${ticks[@]}" ; sleep 1
	for l in "${ticks[@]}"; do
		echo -ne "\e[$l;1H"
		printf -v y "%0.01f" "$(bc<<<"$start_value-(($l-$lcenter)*$scale_factor)")"
		if [ -n "$INT" ] ; then
			printf '%0.0f' "$y"
		fi
		if has_units "$UNIT" ; then
			printf "%s" "$(format_unit "$y" "$UNIT")"
		else
			echo -n "$y"
		fi
	done
	echo -ne "\e[0m"
}
# Display x ticks, at (absolute) line $1, eventually at (absolute) eventually column $2
function disp_x_ticks() {
	local y="$1" x="${1:-}"
	echo -ne "\e[38;5;${HLINE_COLOR}m"
	if [ -n "$x" ] ; then	# if I got only y, I draw all the ticks
		for i in $(seq "${X_TICKS_STEP}" "${X_TICKS_STEP}" "${COLUMNS}") ; do
			echo -ne "\e[$y;${i}H${X_TICK}"
		done
	else	# should I REALLY put a tick?
		if [ "$((x % X_TICKS_STEP))" -eq "0" ] ; then
			echo -ne "\e[$y;${x}H${X_TICK}"
		fi
	fi
	echo -ne "\e[0m"
}
# Clean the screen and draw x axis
function clean_screen() {
	local DT="$1" c="${command_title:-$command}"
	_log "$DT"
	echo -ne "\e[2J\e[?25l"
	if [ -n "$DER" ] ; then
		printf "\e[1;1HEvery %0.02fs: ${MODES[$MODE]} \e[2md/dt(\e[0m\e[4m%s\e[0m\e[2m)\e[0m" "$DT" "${c}"
	else
		printf "\e[1;1HEvery %0.02fs: ${MODES[$MODE]} \e[4m%s\e[0m" "$DT" "${c}"
	fi
	h_line "$lcenter"
}
# Clean the column at (absolute) position $1
function clean_next_column() {
	local x="$1" int_y
	int_y="${dot[$x]:-}"
	if [ -n "$int_y" ] ; then
		echo -ne "\e[$int_y;${x}H "
		if [ "$int_y" -eq "$lcenter" ] ; then	# am I on the x axis?
			echo -ne "\e[$lcenter;${x}H\e[38;5;${HLINE_COLOR}m―\e[0m"
		fi
		# Did I erased a label?
		if [ "$x" -le "$((Y_TICKS_STR_LEN+2))" ] && [[ " ${Y_TICKS_LABSPOS[@]} " =~ "$int_y" ]] ; then
			#disp_y_labels "$int_y"
			disp_y_labels
		fi
	fi
	# (eventually) display the x tick I erase
	disp_x_ticks "$lcenter" "$x"
}
function _usage() {
	local bn="$ME"
	echo " ~ Graphical watch(1) - a.k.a Grafana in a terminal ~"
	echo
	echo "$(color_me white "$bn") [ $(color_me cyan "--sleep") <interval in second> | $(color_me cyan "--window-width") <width in second> ] [ $(color_me cyan "--zero") <value> ] [ $(color_me cyan "--append") <suffix> ] [ $(color_me cyan "--file") <file> ] [ $(color_me cyan "--rainbow") ] [ $(color_me cyan "--mark") <mark> | $(color_me cyan "--matrix") ] [ $(color_me cyan "--title") <command title> ] [ [ $(color_me cyan "--lower") <lower bound> $(color_me cyan "--upper") <upper bound> ] | $(color_me cyan "--scale-factor") <scale> ] [ $(color_me cyan "--unit") <k|m|g|t|p|e|z|y|r|q> | --int ] [ \"command than returns values\" | $(color_me cyan --df) <directory> | $(color_me cyan --du) <directory> ]"
	echo "$(color_me white "$bn") $(color_me cyan "--continue") $(color_me cyan "--file") <file>"
	echo "$(color_me white "$bn") $(color_me cyan "--replay") $(color_me cyan "--file") <file>"
	echo
	echo "$(color_me cyan "--append -a")		$(color_me underlined a)ppend a suffix to the value displayed"
	echo "$(color_me cyan "--continue -c")		$(color_me underlined c)ontinue: load data (values, command) and options ($(color_me cyan "--append"), $(color_me cyan "--slope"), $(color_me cyan "--mark"), $(color_me cyan "--sleep"), $(color_me cyan "--scale-factor"), $(color_me cyan "--title"), $(color_me cyan "--lower"), $(color_me cyan "--upper")) from the $(color_me cyan "--file") file generated in a previous run, then run"
	echo "$(color_me cyan "--df")			Command run is \"$(color_me underlined df) --block-size 1 <directory>\", data gathered is the fourth field: free space, use $(color_me cyan "--unit") M"
	echo "$(color_me cyan "--du")			Command run is \"$(color_me underlined du) --summarize --block-size 1 <directory>\", data gathered is the fourth field: free space, use $(color_me cyan "--unit") M"
	echo "$(color_me cyan "--dump-period -p")	$(color_me underlined p)eriod: dump data every that number of dot. Default is every 5 dot. Valid only with $(color_me cyan "--file")"
	echo "$(color_me cyan "--file -f")		dump data in that $(color_me underlined f)ile upon exit or when SIGHUP is received"
	echo "$(color_me cyan "--int")			round the y label to int"
	echo "$(color_me cyan "--lower -l")		set $(color_me underlined l)ower value"
	echo "$(color_me cyan "--mark -m")		use that one-char string to display $(color_me underlined m)ark"
	echo "$(color_me cyan "--matrix -x")		cyberpunk style: $(color_me underlined "--rainbow") and mark is random"
	echo "$(color_me cyan "--rainbow -r")		$(color_me underlined r)ainbow mode"
	echo "$(color_me cyan "--replay -e")		replay dump specified by $(color_me cyan "--file")"
	echo "$(color_me cyan "--scale-factor -s")	$(color_me underlined s)cale. One line height in the term will count for that many dot. Set to < 1 to zoom in, > 1 to zoom out. Default is 1"
	echo "$(color_me cyan "--sleep -n")		sleep that seconds between each dot. May be decimal. Default is 2s"
	echo "$(color_me cyan "--slope -d")		$(color_me underlined d)iff/$(color_me underlined d)erivate: graph the slope between two dots ( (y2-y1)/sleep ) instead of dots value"
	echo "$(color_me cyan "--title -t")		display that $(color_me underlined t)itle string in status bar instead of the command"
	echo "$(color_me cyan "--unit")			round y label to that nearest unit, 1024-based"
	echo "$(color_me cyan "--upper -u")		set $(color_me underlined u)pper value"
	echo "$(color_me cyan "--window-width -w")	$(color_me underlined w)idth: set the duration of a screen to that many seconds, compute $(color_me cyan "--sleep") accordingly"
	echo "$(color_me cyan "--zero -0")		set the horizontal axis to that value instead of $(color_me underlined 0). If not specified, it will be the first input value"
	echo
	echo "scale and 0 are calculated if --lower and --upper are provided"
	echo
	echo "Examples:"
	echo "	On Linux:"
	echo "	Memory usage:"
	echo "	$bn -n 0.5 -s 100 'free | grep -i mem | awk \"{print \\\$3}\"'"
	echo "	CPU temp:"
	echo "	$bn -n 0.5 -0 43 'cat /sys/class/thermal/thermal_zone0/temp | cut -c -2'"
	echo "	Disk usage of /tmp whose max capacity is 2TB:"
	echo "	$bn --df /tmp/ -l 0 -u \$((2*1024**3))"
	echo
	echo "Show a rainbow bubble sine:"
	echo "	i=0 ; while true ; do LANG=C LC_ALL=C printf '%0.0f\n' \"\$(bc -l <<<\"scale=2;20*s(\$i*(2*6.28)/360)\")\" ; ((i+=1)) ; done | $bn -n 0 -r -m 'o' -t sine"
	echo
	echo "Monitor number of php-fpm processes on a 1-hour graph:"
	echo "	$bn -w 3600 -r -m '-' 'pgrep -c php-fpm'"
	echo
}
# Redraw the previous mark at (absolute) line $1, (absolute) column $2, with color $3
function correct_last_mark() {
	local last_y="$1" last_x="$2" last_mc="$3"
	if [ -z "${last_y}" ]; then
		return
	fi
	echo -ne "\e[${last_y};${last_x}H\e[38;2;${last_mc}m$(mark)\e[0m"
	# Drawing all labels is questionnable...
	if [ "$last_x" -le "$((Y_TICKS_STR_LEN+2))" ] ; then
		disp_y_labels
	fi
}
function disp_status() {
	local min="$1" value="$2" max="$3" scale_factor="$4" x="$5" y="$6" avg="$7" der="$8" suffix
	# status line
	# I don't use 'color_me': it's too slow
	if has_units "$UNIT" ; then
		min="$(format_unit "$min" "$UNIT")"
		max="$(format_unit "$max" "$UNIT")"
		avg="$(format_unit "$avg" "$UNIT")"
		der="$(format_unit "$der" "$UNIT")"
	fi
	# Eventually add "/s"
	suffix="$SUFFIX${DER:+/s}"
	der="$der${SUFFIX}s⁻¹"
	#printf "\e[2;1H\e[2mm:\e[0m%s \e[2mM:\e[0m%s \e[2mwidth:\e[0m%s \e[2mtick:\e[0m%0.01fs \e[2mscale:\e[0m%0.01fx \e[2mx:\e[0m%i \e[2my:\e[0m%i \e[2mfile:\e[0m%s \e[2mpid:\e[0m%i\e[0K" "$min" "$max" "$(s2dhms $WINDOW_WIDTH)" "$X_TICKS_WIDTH" "$scale_factor" "$x" "$int_y" "${DUMP_FILE:-<none>}" "$$"
	printf "\e[2;1H\e[2mm:\e[0m%s%s \e[2mM:\e[0m%s%s \e[2m~:\e[0m%s%s \e[2md:\e[0m%s \e[2mwidth:\e[0m%s \e[2mtick:\e[0m%0.01fs \e[2mfile:\e[0m%s \e[2mpid:\e[0m%i\e[0K" "$min" "$suffix" "$max" "$suffix" "$avg" "$suffix" "$der" "$(s2dhms $WINDOW_WIDTH)" "$X_TICKS_WIDTH" "${DUMP_FILE:-<none>}" "$$"
	# value
	if has_units "$UNIT" ; then
		value="$(format_unit "$value" "$UNIT")"
	fi
	printf "\e[1;$((hcenter-(${#value}+3+4+suffix_length-1)/2))H [ \e[1;37m%s%s\e[0m ] " "$value" "$suffix"
	# date
	printf "\e[1;${DATE_COLUMNS}H%s: %s" "$HOSTNAME" "$($date -Iseconds)"
}
function value_to_y() {
	local v="$1"
	bc <<< "scale=1;$lcenter - ( ($v-$start_value)/$scale_factor )"
}
function redraw() {
	local till="$1" x
	for x in $(seq 1 "$((till-1))") ; do
		# Store the new rescaled coordinate
		dot[x]="$( printf '%0.0f' "$( value_to_y "${dotv[x]:-}")" )"
		rgb="$(( (x + RGB_start) % ${#RGB[*]} ))"
		echo -ne "\e[${dot[x]};${x}H\e[38;2;${RGB[rgb]}m$(mark)\e[0m"
	done
}
function _dump() {
	_log "Dumping to $DUMP_FILE..." ; touch "$KEEP_LOG_FILE"
	local data logs
	# Oneliner(c)(r)(tm)
	data="$(jq -c -s '.' "$VALUES_FILE")"
	# https://stackoverflow.com/questions/44792241/constructing-a-json-object-from-a-bash-associative-array
	# logs="$(printf '%s\0' "${!LOGS[@]}" "${LOGS[@]}" | jq -Rs 'split("\u0000") | . as $v | (length / 2) as $n | reduce range($n) as $idx ({}; .[$v[$idx]]=($v[$idx+$n]|tonumber))|to_entries')"
	# Probably slow
	source "$LOG_DUMP_FILE"
	logs="$(for k in "${!LOGS[@]}" ; do jo -- ts_nano="$k" value="${LOGS[$k]}"; done | jq -c -s '.' )"
	jo infos="$(jo -- dump_version=1 hostname="$(hostname)" command="$command" command_title="${command_title:-}" pid="$$" start_time="$($date -d@$START_TIME)" start_time_ts="$START_TIME" lines="$LINES" columns="$COLUMNS" scale="$scale_factor" sleep="$SLEEP" mark="$MARK" upper="$MAX" lower="$MIN" suffix="$SUFFIX" der="${DER:-}" -b rainbow="$RAINBOW" )" data="$data" logs="$logs" > "$DUMP_FILE"
	_log "" ; rm -f "$KEEP_LOG_FILE"
}
function _load_data() {
	local f="$1"
	if [ -e "$f" ] ; then
		jq '.data//{} | .[]  | .value' "$f"
	fi
}
function _exit() {
	# cursor visible and set me at the end of the screen
	echo -ne "\e[?25h\e[$LINES;1H\e[0m"
	if [ -n "$DUMP_FILE" ] && [ -z "$REPLAY" ] ; then
		_dump
	fi
	rm -f "$VALUES_FILE" "$LOG_DUMP_FILE"
	echo "Waiting for jobs to finish"
	wait
}
function _draw() {
	while read -r fvalue ; do
		ts_nano="$($date "+%s.%N")"
		if ! is_float "$fvalue" ; then
			asc="$(printf "%02.2X" "'$fvalue'")"
			if [[ $asc -ne 27 ]] ; then				# if it's not EOF
				_log "'$fvalue' (0x$asc) is not at least a float"
				continue
			else
				echo "Got empty value"
				exit 0
			fi
		else
			# int of the value, to be used in later comparison
			printf -v value '%0.0f' "$fvalue"
		fi
		x="$col"
		if [ -n "$DER" ] ; then
			local der
			# Derivate mode. SLEEP=0, I have to count the time
			# between each data
			if [[ $SLEEP -eq 0 ]] ; then
				DT="$(bc -l <<< "$ts_nano-$last_value_s")"
				#_log "$fvalue - $DER / d = $der"
			else
				DT="$SLEEP"
			fi
			der="$(bc <<<"scale=2;($fvalue-$DER)/$DT")"
			#_log "$fvalue - $DER / $DT = $der"
			y="$(value_to_y "$der")"
			printf -v value '%0.0f' "$der"
			# Store old value
			DER="$fvalue"
		else
			# value => line (eventualy a float value)
			y="$(value_to_y "$fvalue")"
			#DT="$SLEEP"
			DT="$(bc -l <<< "$ts_nano-$last_value_s")"
		fi
		last_value_s="$ts_nano"
		printf -v int_y '%0.0f' "$y"
		if [ "$value" -lt "$min" ] ; then
			min="$value"
		fi
		if [ "$value" -gt "$max" ] ; then
			max="$value"
		fi
		if [ "$int_y" -le "$HEADER_SIZE" ] || [ "$int_y" -ge "$LINES" ] ; then
			# Compute new scale factor, multiplied by 1.2 to add room on top and bottom
			scale_factor="$(bc <<<"scale=2;1.2*($value-($start_value))/($LINES-$HEADER_SIZE-$lcenter)" | tr -d '-' )"
			clean_screen "$DT"
			redraw "$col"
			# Redraw y labels, as they may have been erased by the dots
			disp_y_labels
			#disp_status "$min" "$fvalue" "$max" "$scale_factor" "$x" "$y"
			y="$(value_to_y "$fvalue")"
			printf -v int_y '%0.0f' "$y"
			_log "Set scale to $scale_factor"
		else
			correct_last_mark "$last_y" "$last_x" "$last_mc"
		fi
		# next color: next value in the array, wrapping
		rgb="$(( (n + RGB_start) % ${#RGB[*]} ))"
		# To allow _dump to make its job on exit
		# At least date(1) is portable...
		jo ts_nano="$ts_nano" value="$fvalue" >> "$VALUES_FILE" &
		echo -ne "\e[${int_y};${x}H\e[38;2;${RGB[$rgb]}m\e[7m${MARK_TIP}\e[0m"
		# store position of the dot (to delete it next time)
		dot[x]="$int_y"
		if [[ -n "$DER" ]] ; then
			# store the value of that point, without notion of scale
			dotv[x]="$der"
		else
			dotv[x]="$fvalue"
		fi
		# Oneliner to compute average
		avg="$(awk 'BEGIN{RS=" "}{if (NR>1) {s+=$1; n+=1}}END{printf "%.2f",s/n}'<<<"${dotv[@]}")"
		deriv="$(bc <<<"scale=2;($fvalue-(${dotv[$((x-1))]}))/$DT")"
		disp_status "$min" "${dotv[$x]}" "$max" "$scale_factor" "$x" "$y" "$avg" "$deriv"
		last_x="$x"
		last_y="$int_y"
		last_mc="${RGB[$rgb]}"
		col="$(( 1 + col % COLUMNS ))"
		clean_next_column "$(( 1 + col % COLUMNS))"
		((n+=1))
		# No need to dump if I'm in replay mode, or no dump asked, or .. it's not the time
		if [ -z "$REPLAY" ] && [ -n "$DUMP_FILE" ] && [ $((n%DUMP_PERIOD)) == "0" ] ; then
			_dump &
		fi
	done
	_log "No more data"
}
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
options=$(getopt -o '0:a:cdef:hl:m:n:p:rs:t:u:w:x' --long replay,zero,scale-factor:,matrix,continue,file:,slope,dump-period:,title:,window-width:,upper:,lower:,rainbow,help,mark:,int,unit:,df:,du: -- "$@")
eval set -- "$options"
#while getopts '0:a:cdef:hl:m:n:p:rs:t:u:w:x' opt ; do
while true ; do
	case $1 in		# Refactor this, please
		-0|--zero)			shift; is_float "$1" && start_value="$1";;
		-a|--append)		shift; SUFFIX="$1";;
		-c|--continue)		CONTINUE='y';;
		--df)				shift; DF="$1";;
		--du)				shift; DU="$1";;
		-d|--slope)			DER='0';;
		-e|--replay)		MODE="play"; REPLAY="y" ; CONTINUE="y";;
		-f|--file)			shift; DUMP_FILE="$1" ; if ! [ "$MODE" = "play" ] ; then MODE="record" ; fi;;
		-h|--help)			_usage ; exit 0;;
		-l|--lower)			shift; is_float "$1" && MIN="$1";;
		-m|--mark)			shift; MARK=("$1");;
		-n|--sleep)			shift; is_float "$1" && SLEEP="$1";;
		-p|--dump-period)	shift; DUMP_PERIOD="$1";;
		-r|--rainbow)		RAINBOW='y';;
		-s|--scale-factor)	shift; is_float "$1" && scale_factor="$1";;
		-t|--title)			shift; command_title_default="$1";;
		-u|--upper)			shift; is_float "$1" && MAX="$1";;
		-w|--window-width)	shift; is_float "$1" && SLEEP="$( bc<<<"scale=2;$1/$COLUMNS" )";;
		-x|--matrix)		RAINBOW='y' ; MARK=( "'" '"' "!" "#" "$" "%" "(" ")" "*" "+" "," "-" "." "/" ":" ";" "<" ">" "=" "?" "@" "[" "]" "^" "_" "{" "}" "|" "~" "~" {a..z} {A..Z} {0..9} );;
		--int)				INT='y';;
		--unit)				shift; UNIT="$1";;
		--) shift; break;;
	esac
	shift
done
preflight_check || exit 1
shift $((OPTIND-1))
if [ -n "$DUMP_FILE" ] ; then
	_check_command jq || exit 1
	trap _dump 1
fi
if [ -n "$CONTINUE" ] && [ -e "$DUMP_FILE" ] ; then
	echo "Load data and parameters from $DUMP_FILE"
	values=( $(_load_data "$DUMP_FILE") ) ; jq -c '.data//{} | .[]' "$DUMP_FILE" > "$VALUES_FILE"
	# TODO : load logs
	start_value="${values[0]}"
	command="$(jq -r '.infos.command' "$DUMP_FILE")"
	command_title="$(jq -r '.infos.command_title//""' "$DUMP_FILE")"
	# Allow some options to be changed
	MARK=( "${MARK[@]:-$(jq -r '.infos.mark' "$DUMP_FILE")}" )
	SLEEP="${SLEEP:-$(jq -r '.infos.sleep' "$DUMP_FILE")}"
	RAINBOW="${RAINBOW:-$(jq -r '.infos.rainbow//""' "$DUMP_FILE")}"
	MIN="${MIN:-$(jq -r '.infos.lower//""' "$DUMP_FILE")}"
	MAX="${MAX:-$(jq -r '.infos.upper//""' "$DUMP_FILE")}"
	SUFFIX="${SUFFIX:-$(jq -r '.infos.suffix//""' "$DUMP_FILE")}"
	DER="${DER:-$(jq -r '.infos.der//""' "$DUMP_FILE")}"
	scale_factor="${scale_factor:-$(jq -r '.infos.scale' "$DUMP_FILE")}"
	n="${#values[@]}"
else
	n=0
	values=()
	if [ -z "${1:-}" ] ; then
		if [ -n "${DF:-}" ] ; then
			command="df --block-size 1 \"$DF\" | tail -1 | awk '{print \$4}'"
			UNIT="${UNIT:-M}"
			command_title="df \"$DF\""
		elif [ -n "${DU:-}" ] ; then
			command="du --summarize --block-size 1 \"$DU\" | awk '{print \$1}'"
			UNIT="${UNIT:-M}"
			command_title="du \"$DU\""
		else
			echo 'Read from stdin'
			command="read -r v ; echo \$v"
			command_title="${command_title:-<stdin>}"
			SLEEP=0
		fi
	else
		command="$*"
	fi
fi
if [ -n "$MIN$MAX" ] ; then		# Test if MIN and MAX are not empty
	if [ "$MIN$MAX" == "$MIN" ] || [ "$MIN$MAX" == "$MAX" ] ; then	# Test if MIN or MAX is empty
		echo "-l and -u MUST be specified together" >&2
		exit 2
	elif [ "$MIN" -lt "$MAX" ] ; then
		printf -v start_value '%0.0f' "$(bc -l <<<"scale=2;($MAX+($MIN))/2")"
		scale_factor="$( bc <<<"scale=2;($MAX-($MIN))/$LINES" )"
	else
		echo "-l MUST BE strictly lower that -u" >&2
		exit 2
	fi
fi
trap _exit 0
SLEEP=${SLEEP:-$SLEEP_DEFAULT}
WINDOW_WIDTH="$(bc<<<"$SLEEP*$COLUMNS")"
X_TICKS_WIDTH="$(bc<<<"$X_TICKS_STEP*$SLEEP")"
# Can't use the m=${m:-v} trick as it's an array
if [ -z "${MARK:-}" ] ; then
	MARK=( "${MARK_DEFAULT[@]}" )
fi
SUFFIX="${SUFFIX:-$SUFFIX_DEFAULT}"
scale_factor=${scale_factor:-$scale_factor_default}
command_title="${command_title:-$command_title_default}"
col=1
suffix_length="${#SUFFIX}"
# Tiny hack to have the "read-from-stdin" command
# Read value while I don't have an int
if [ -n "$DER" ] ; then
	# Set to 0 in derivate mode
	start_value=0
else
	start_value="$(bash -c "$command")"
	while ! is_float "$start_value" ; do
		start_value="$(bash -c "$command")"
	done
fi
# size of a tick label, in char
Y_TICKS_STR_LEN="$(wc -c <<< "$start_value" )"

# Tiny hack to avoid useless computation
if [ "${#MARK[@]}" -gt 1 ] ; then
	function mark() {
		echo -n "${MARK[$(( ($RANDOM % ${#MARK[@]}-1)))]}"
	}
else
	function mark() {
		echo -n "${MARK[0]}"
	}
fi
printf -v start_value '%0.0f' "$start_value"
min="$start_value"
max="$start_value"

clean_screen "$SLEEP"
if [ -n "$RAINBOW" ] ; then
	_log "Generating rainbow table..."
	RGB=( $(generate_rainbow 15) )
	RGB_start=$(( RANDOM % ${#RGB[*]} ))	# I'll start somewhere random
else
	RGB=( $MARK_COLOR )
	RGB_start=0
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ -n "$REPLAY" ] ; then		# Replay mode
	command_title="Replay"
	for fvalue in "${values[@]}" ; do
		echo "$fvalue"
		sleep "$SLEEP"
	done | _draw
else	# Real time mode
	last_value_s="$($date "+%s.%N")"
	# If I'm in derivate mode, first value will not be drawn
	if [ -n "$DER" ] ; then
		DER="$(bash -c "$command")"
		sleep "$SLEEP"
	fi
	while true ; do
		# Floating value
		bash -c "$command"
		sleep "$SLEEP"
	done | _draw
fi
